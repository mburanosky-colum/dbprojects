#### README:

##### Description:
pyRepair is Python code that repairs tables damaged by the Accumail Frameworks 
application due to excessive new line characters. It reads in delimiter-separated flat files.

##### Dependencies:

  1. [Python2](https://www.Python.org/)

##### Configuration:

Edit ```pyRepair\config.py``` prior to running script to change 
the input file paths, the output file paths and delimiter.

##### Run code:
```
python pyCutNewline.py  
```

##### Tests:

  1. **Test1** 
    Descr.: Multiple line breaks between table values, after delimiter, and after expected line break
    Status: Success 
  2. **Test2**
    Descr.: Header with no rows or line breaks
    Status: Success 
  3. **Test3** 
    Descr.: Line break between first column value
    Status: Failure (Raises ValueError: 'Multiple new line characters')
  4. **Test4**
    Descr.: Line break between table value
    Status: Success
  5. **Test5** 
    Descr.: No excessive or misplaced line breaks
    Status: Success
  6. **Test6**
    Descr.: No line break after delimiter where it is expected
    Status: Failure (Raises ValueError: 'Expected new line character')  	


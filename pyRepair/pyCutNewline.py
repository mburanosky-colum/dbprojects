import re
from config import * 

# Repair text files damaged
# by Accumail Frameworks application
#	1. Read in table as a regular expression split by delimiter 
# 	2. Identify values where newline characters are expected based on column count
#	3. Raise an error if a newline character does not appear in an expected value
# 	4. Remove newline characters that are out of place
#	5. Replace repeated newline characters with just one
#	6. Raise an error if multiple newline characters appear separated in the first or last column
#                   (In this case, the user has to chose which newline character to keep)

def main(input_file, output_file):
  
  # save entire file to string
  with open(input_file) as fin: string = fin.read()

  #count number of delimiters appearing before first newline character
  first_newline_val = string.split('\n')[0].count(delim)
  exp_next_newline_val = first_newline_val

  # split values by delimiter
  delim_str = string.split(delim)

  # populate list with indexes of values that should contain a newline character
  next_val_idx = first_newline_val
  newline_val_idx = []
  while next_val_idx < len(delim_str):
    newline_val_idx.append(next_val_idx)
    next_val_idx += exp_next_newline_val

  # remove bottom right value in table from newline value index
  newline_val_idx = newline_val_idx[:-1]

  # get list of strings expected to contain a newline value
  list_with_expected_newline_val = [delim_str[item] for item in newline_val_idx]

  # raise error if there's not a newline character where there should be
  for item in list_with_expected_newline_val:
    if '\n' not in item: raise ValueError('Expected new line character')

  # get a list of indexes in delimited string
  all_idx = [k for k in range(len(delim_str))]

  # get a list of regular (non-newline) value indexes
  reg_val_idx = list(set(all_idx) - set(newline_val_idx))

  # get list of strings not expected to contain a newline value
  list_with_regular_val = [delim_str[item] for item in reg_val_idx]

  # remove unexpected newline values
  list_with_regular_val = map(lambda x : x.replace('\n', ''), list_with_regular_val)

  # re-construct table with unexpected newline characters removed
  reconstructed_table = list_with_regular_val
  for k in range(len(newline_val_idx)):
    reconstructed_table.insert(newline_val_idx[k], list_with_expected_newline_val[k])

  # remove repeated newline characters
  reconstructed_table = map(lambda x : re.sub(ur"(\n)\1+", r"\1", x), reconstructed_table)
  
  # Raise an error if there remain repeated newline characters
  for item in reconstructed_table:
    if len(re.findall(r'\n', item)) > 1: raise ValueError('Multiple new line characters')

  # join values in reconstructed table with delimiter
  reconstructed_table = delim.join(reconstructed_table)
  
  # write table to file
  file = open(output_file, 'w')
  file.write(reconstructed_table)
  file.close()
  return;
  
if __name__ == '__main__':

  # iterate through input and output files selected in config file
  for input_file, output_file in zip(input_files, output_files):
    main(input_file, output_file);
